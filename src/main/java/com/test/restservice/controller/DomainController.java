package com.test.restservice.controller;

import com.test.restservice.dto.ResponseDto;
import com.test.restservice.dto.SearchDto;
import com.test.restservice.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DomainController {
    @Autowired
    private DomainService domainService;

    @GetMapping("/domains/check")
    public List<ResponseDto> check(SearchDto searchDto) {
        String search = searchDto.getSearch();

        return domainService.getDomains(search);
    }
}
