package com.test.restservice.repository;

import com.test.restservice.model.Tld;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TldRepository extends JpaRepository<Tld, Long> {
}
