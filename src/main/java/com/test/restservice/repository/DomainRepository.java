package com.test.restservice.repository;

import com.test.restservice.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DomainRepository extends JpaRepository<Domain, Long> {
    List<Domain> queryAllByNameStartingWith(String name);
}
