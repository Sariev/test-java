package com.test.restservice.service.impl;

import com.test.restservice.dto.ResponseDto;
import com.test.restservice.model.Domain;
import com.test.restservice.model.Tld;
import com.test.restservice.repository.DomainRepository;
import com.test.restservice.repository.TldRepository;
import com.test.restservice.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DomainServiceImpl implements DomainService {
    @Autowired
    private DomainRepository domainRepository;

    @Autowired
    private TldRepository tldRepository;

    @Override
    public List<ResponseDto> getDomains(String name) {
        List<Domain> domainList = domainRepository.queryAllByNameStartingWith(name);
        Map<String, Boolean> domainMap = new HashMap<>();
        domainList.forEach(domain ->{
                String[] existingDomain = domain.getName().split("\\.");
                if (existingDomain.length == 1){
                    domainMap.put("com", !existingDomain[0].equals(name));
                } else {
                    domainMap.put(existingDomain[1], !existingDomain[0].equals(name));
                }
            }
        );

        List<Tld> tldList = tldRepository.findAll();

        List<ResponseDto> responseDtoList = new ArrayList<>();

        tldList.forEach(tld ->
            {
                Boolean isAvailable = domainMap.get(tld.getName());

                ResponseDto domainDto = new ResponseDto(
            name + "." + tld.getName(),
                    tld.getName(),
                    isAvailable == null ? true : isAvailable,
                    tld.getPrice()
                );

                responseDtoList.add(domainDto);
            }
        );

        responseDtoList.sort(Comparator.comparing(ResponseDto::getPrice));

        return responseDtoList;
    }
}
