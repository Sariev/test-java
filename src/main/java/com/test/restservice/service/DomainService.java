package com.test.restservice.service;

import com.test.restservice.dto.ResponseDto;

import java.util.List;

public interface DomainService {
    List<ResponseDto> getDomains(String name);
}
